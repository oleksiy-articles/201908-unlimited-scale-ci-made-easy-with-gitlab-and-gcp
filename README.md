# "Unlimited" scale CI made easy with GitLab and GCP

I want to share a small workflow which allows you to scale your CI workflows [almost] for free. Scale is not really *unlimited* - see [GCP CPU Limitations](#gcp-cpu-limitations) for more details.

![Gitlab CI/CD](https://about.gitlab.com/images/ci/ci-cd-test-deploy-illustration_2x.png)

## What is required:
- Google Cloud Platform (GCP) account, preferably separate one for CI only.
- Host your repository on GitLab to utilize its CI pipelines.

GitLab not only provides powerful CI tools for free but [which is even more important] out of the box - configured and ready to work.

## Issues

1. GitLab calculates computing time on shared workers and for free you will get only 2000 minutes.

1. you have no control over shared workers, their performance and availability which may be a bigger problem than the first one.

## How fix this?

Here comes GCP. You do not have to use GCP, in fact, any hosting provider with API [supported by GitLab Runner will work](https://docs.gitlab.com/runner/configuration/autoscale.html#supported-cloud-providers). In fact, [supported by Dockker Machine](https://docs.docker.com/machine/drivers/). GCP worked for me the best.

Using GCP account:
- Create runner manager instance for "always free".
- Create runner worker instances on demand with very predictable and granulated pricing.

## Runner instance

1. Create [always free](https://cloud.google.com/free/docs/gcp-free-tier) instance. Which should be `f1-micro` in one of `us-*` zones. Make sure you double check [always free usage limits](https://cloud.google.com/free/docs/gcp-free-tier#always-free-usage-limits) for details.
   - OS *is not* really important for runner manager instance. I usually choose latest LTS Ubuntu in this case to max tools avialability.
   - grant **read/write** access in *Cloud API access scopes* section for *Compute Engine* to make sure this instance will be able to create and destroy other instances in your GCP project.

2. [Install GitLab Runner](https://docs.gitlab.com/runner/install/linux-manually.html) on it.

3. [Register your runner with your repository](https://docs.gitlab.com/runner/register/) or project/organization.
   - use `docker+machine` type
   - this will generate `[[runners]]` section for you with token
   - you can register multiple runners from same management instance to work with multiple configurations if needed

## `config.toml` and worker instances

Usually your configuration path is `/etc/gitlab-runner/config.toml` or smth like this.

### Worker instance configuration

- For CI it is common to be limited by CPU, so in most cases you should `n1-highcpu-*` type. But [GCP gives full flexibility here](https://cloud.google.com/compute/docs/machine-types).

- Last time I checked Gitlab Runner have **not** supported `Container-Optimized` images from Google. So that, smallest OS you can use is CoreOS images

- You should not use shared CPU instances unless you do not really care about worker env consistency. BTW, in many CI workloads you *do not*. This was tested and shared CPU instances performance is not predictable (as it is promised by GCP).

More details will follow config example.

### GCP CPU Limitations

**BE AWARE** of [GCP Quotas](https://cloud.google.com/compute/quotas) and that on GCP credit you have only 8 total CPU units for all instances.

Once, you upgrade to full account (preserving credit) - you will have 24 total CPU units limit. And tyou can create [special support request to increase this quota](https://cloud.google.com/compute/quotas#request_quotas).

**IMPORTANT** here is that Gitlab Runner cannot detect when you exceed CPU limit and machine cannot be created and just stucks.

[Plan configuration (instance types vs concurrency) accordingly](https://docs.gitlab.com/runner/configuration/autoscale.html#how-concurrent-limit-and-idlecount-generate-the-upper-limit-of-running-machines) not to exceed
your CPU units.

Check logs (for CoreOS, `/var/log/syslog`) in case you suspect gitlab runner is not starting instances for you properly.

### `config.toml` detailed

```toml
concurrent = 2 # this is amount of concurrent tasks to be invoked
               # see CPU units limit note above
check_interval = 0 # seconds, 0 is actually not 0
                   # but default value which is 3 or smth

[session_server] # just defaults
  session_timeout = 1800

# here are runner types we want to launch
# i.e. GitLab can run several runners of each type
# usually you need only one type

# this section is pre-generated for you when you register runner
# but you can modify any pre-configuration to this reference
[[runners]]
  name = "runner-2cpu"
  url = "https://gitlab.com/"
  token = "TOKEN_PROVIDED"
  executor = "docker+machine" # this is type we need for remote execution
  # environment = ["DOCKER_TLS_CERTDIR="] # see note below
  limit = 0 # this is limit of runners of this type
            # 0 means no limit
            # sometimes you want 1 runner of a type (and different runners)
            # this is tightly coupled to concurrency and CPU units you have
  [runners.docker] # this section is mostly fromrunner registration step
    tls_verify = false
    image = "docker:latest"
    privileged = true # you should have this to true in this use case
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0
  [runners.machine] # this is configuation of future runners
    IdleCount = 0 # set > 0 if you want some instance(s) to be always online
                  # I do not see any sense of it in CI as it is created
                  # in GCP really fast
    IdleTime = 0  # seconds, set this time to keep instance alive some time after
                  # job finishes, this may help to reuse instance betweem CI stages
                  # without spending time on recreation
                  # 60 seconds should be enough for this example
    MachineDriver = "google" # select GCP
    MachineName = "ci-2cpu-%s" # machine name where `%s` will be replace with uniq hash on runner start
    MachineOptions = [
      "google-project=project-id", "google-machine-type=n1-highcpu-2", "google-machine-image=coreos-cloud/global/images/family/coreos-stable", "google-tags=instance-tag", "google-preemptible`=true", "google-zone=us-east1-c", "google-use-internal-ip=true"]
    OffPeakTimezone = "" # I disabled offpeak features because
    OffPeakIdleCount = 0 # do not see any real use cases for them
    OffPeakIdleTime = 0  # in CI workloads

# there may be more [[runners]] sections if you register multiple runners
```

#### `environment = ["DOCKER_TLS_CERTDIR="]`

Since Docker 18.09.8 you cannot use `docker+machine` with `docker:dind` with default settings.

There are two options:

1. disable TLS by promoting `environment = ["DOCKER_TLS_CERTDIR="]`
   but this will still show you warnings on healthchecks on each job start

2. lock down to version 18.09.7 which works as expected without TLS changes
> for ex., you can do it this way in  your project's `.gitlab-ci.yml`:
> ```yaml
> image: docker:${DOCKER_VERSION}
> services:
>  - docker:${DOCKER_VERSION}-dind
> variables:
>   DOCKER_VERSION: '18.09.7'
> 
> ...
> ```

I recommend [this DinD-related disscussion](https://gitlab.com/gitlab-org/gitlab-runner/issues/1986)  to follow up the issue.

#### Machine options

Here are more details to machine options settings because error messages you can find in Gitlab runner logs are not always clear:

`google-project` is just an unique ID of your project where you have access to start new instances via API (see about access above in [Runner instance #1->grant](#runner-instance) section).

`google-zone` is one of your project's zones

`google-machine-type` you can use any of [standard predefined types](https://cloud.google.com/compute/docs/machine-types) or even any [valid custom type](https://cloud.google.com/compute/docs/instances/creating-instance-with-custom-machine-type) (see GCloud example how to compose custom type into one expression)

> depending on `google-zone` second (new!) generation of instances may be available

`google-machine-image` is a little tricky - you have [a list of available images](https://cloud.google.com/compute/docs/images) but to compose valid value for this parameter I'd recommend:
> use command
> ```bash
> > gcloud compute images list --uri
>
> ...
> https://www.googleapis.com/compute/v1/projects/coreos-cloud/global/images/coreos-beta-2219-2-1-v20190828
> https://www.googleapis.com/compute/v1/projects/coreos-cloud/global/images/coreos-stable-2135-6-0-v20190801
> https://www.googleapis.com/compute/v1/projects/cos-cloud/global/images/cos-69-10895-348-0
> ...
> ```
> and extract part after projects and removing version specific suffix
>
> i.e.
> ```
> https://www.googleapis.com/compute/v1/projects/coreos-cloud/global/images/coreos-stable-2135-6-0-v20190801
> ```
>
> becomes
> ```
> coreos-cloud/global/images/coreos-stable
> ```

`google-tags` is how you want to tag your instances for accountability

`google-preemptible` we usually want `true` to use cheap instances

`google-use-internal-ip` you usually want it to `true` not to pay for external IPs [which are not free any more](https://cloud.google.com/compute/all-pricing#ipaddress).
NOTE: this option doesn't seem to work with latest GCP API

## At the end of the day

  - you will have Gitlab CI/CD process
  - it'll be connected to your runner manager instance
  - and for each pipeline/task new worker instance will be created because of `docker+machine` runner type
  - task will be executed
  - runner worker instance will be deleted
  - you will be charged only for actual innstance usage time with preemptible rate

Thanks and have a great CI pipelines!
